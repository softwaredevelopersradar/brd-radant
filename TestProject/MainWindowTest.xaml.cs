﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using CompassModelLibrary.Devices;
using ModelsTablesDBLib;

namespace TestProject
{
    using CompassModelLibrary;
    using CompassModelLibrary.Com;
    using CompassModelLibrary.Enums;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindowTest : Window
    {

        private const string RRS1 = "RRS1";
        private const string RRS2 = "RRS2";
        private const string LPA = "LPA";
        private const string LPAU = "LPAU"; // управляемый

        private List<ComParam> comParam = new List<ComParam>();

        List<Button> btnList = new List<Button>();

        public MainWindowTest()
        {
            InitializeComponent();
            LocalProperties properties = new LocalProperties()
            {
                ARD1 = new ARDConnection()
                {
                    Address = 1,
                    Type = TypeRSD.Radant,
                    Device = DeviceRSD.SSTU,
                    ComPort = "COM16",
                    PortSpeed = 9600,
                    State = true,
                    IsVisible = true,
                    Level = 1
                }
            };
            CompassDeviceFactory factory = new CompassDeviceFactory(properties);
            comParam = new List<ComParam>
            {
                new ComParam()
                {
                    PortName = "COM17",
                    BaudRate = 9600,
                    Parity = System.IO.Ports.Parity.None,
                    DataBits = 8,
                    StopBits = System.IO.Ports.StopBits.One,
                },

                new ComParam()
                {
                    PortName = "COM2",
                    BaudRate = 9600,
                    Parity = System.IO.Ports.Parity.None,
                    DataBits = 8,
                    StopBits = System.IO.Ports.StopBits.One,
                },

                new ComParam()
                {
                    PortName = "COM3",
                    BaudRate = 9600,
                    Parity = System.IO.Ports.Parity.None,
                    DataBits = 8,
                    StopBits = System.IO.Ports.StopBits.One,
                },

                new ComParam()
                {
                    PortName = "COM4",
                    BaudRate = 9600,
                    Parity = System.IO.Ports.Parity.None,
                    DataBits = 8,
                    StopBits = System.IO.Ports.StopBits.One,
                },

                new ComParam()
                {
                    PortName = "COM5",
                    BaudRate = 9600,
                    Parity = System.IO.Ports.Parity.None,
                    DataBits = 8,
                    StopBits = System.IO.Ports.StopBits.One,
                },

            };

            BRDRadantGr.Radants = factory.CreateDeviceObject();
            BRDRadantGr.SetLanguage((CompassControl.Enums.Languages)Languages.Rus);

            foreach (CompassModel obj in BRDRadantGr.Radants)
            {
                obj.Device.OpenPort();
                

                #region TestButton
                var btn = new Button();

                btn.Margin = new Thickness(0, 0, 0, 0);
                btn.Background = new BrushConverter().ConvertFromString("#FF424141") as SolidColorBrush;
                btn.HorizontalAlignment = HorizontalAlignment.Stretch;
                btn.Content = (BRDRadantGr.Radants.IndexOf(obj) + 1).ToString();
                btn.Foreground = Brushes.White;
                btn.Tag = BRDRadantGr.Radants.IndexOf(obj);


                btn.Click += (source, e) =>
                {
                    if (BRDRadantGr.Radants[BRDRadantGr.Radants.IndexOf(obj)].State == true)
                        BRDRadantGr.Radants[BRDRadantGr.Radants.IndexOf(obj)].Device.ClosePort();
                    else
                        BRDRadantGr.Radants[BRDRadantGr.Radants.IndexOf(obj)].Device.OpenPort();
                };

                gridBtn.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(50, GridUnitType.Pixel) });
                gridBtn.Children.Add(btn);
                Grid.SetColumn(btn, gridBtn.ColumnDefinitions.Count - 1);

                BRDRadantGr.Radants[BRDRadantGr.Radants.IndexOf(obj)].Device.OnOpenPort += ShowOpenPort;
                BRDRadantGr.Radants[BRDRadantGr.Radants.IndexOf(obj)].Device.OnClosePort += ShowClosePort;

                btnList.Add(btn);

                #endregion
            }
        }


        private void ShowOpenPort(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                var buttons = gridBtn.Children.OfType<Button>().ToList();
               
                buttons[BRDRadantGr.Radants.IndexOf(sender as CompassModel)].Background = new BrushConverter().ConvertFromString("#008000") as SolidColorBrush;

            }));
        }

        private void ShowClosePort(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                var buttons = gridBtn.Children.OfType<Button>().ToList();

                buttons[BRDRadantGr.Radants.IndexOf(sender as CompassModel)].Background = new BrushConverter().ConvertFromString("#FF0000") as SolidColorBrush;

            }));
           

        }

        private void ShowReadByte(object sender, ByteEventArgs e)
        { }
        private void ShowWriteByte(object sender, ByteEventArgs e)
        { }


        private async void Test_Click(object sender, RoutedEventArgs e)
        {
            BRDRadantGr.Radants[0].Course = 7;
            int i = 0;

            while (i < 10)
            {
                BRDRadantGr.Radants[0].Angle++;
                i++;

                await Task.Delay(300);
            }


        }

    }
}
