﻿namespace CompassModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Globalization;
    using System.Runtime.CompilerServices;
    using System.Threading;
    using System.Windows.Media;
    using Com;
    using Events;
    using Interface;
    using ModelsTablesDBLib;

    public  class CompassModel : ComBRD, IBRD, INotifyPropertyChanged
    {
        #region Const

        private const byte _MAX_TIER = 5;

        #endregion

        #region Field

        private LocalProperties properties;

        private string _name;
        private string _nameDevice = "";

        private float _angle;
        private float _elevation;

        private float _сorrect;
        private float _сourse;

        private float _direct;

        private byte _tier;
        private List<float> _tierAngle;

        #endregion



        #region Property


        [Category("ARD")]
        [Browsable(false)]
        public string NameDevice // задается лишь раз
        {
            get => this._nameDevice;
            set
            {
                if (this._nameDevice != "") return;
                if (this._nameDevice == value) return;
                this._nameDevice = value;
            }
        }

        [Category("ARD")]
        [DisplayName("Name")]
        [Description("ARD name"), ReadOnly(true), Browsable(true)]
        public string Name
        {
            get=>this._name;
            set
            {
                if (this._name == value) return;
                this._name = value;

                this.OnPropertyChanged();
            }
        }

        [Category("ARD")]
        [DisplayName("Tier")]
        [Description("ARD tier"), ReadOnly(true)]
        public byte Tier
        {
            get => this._tier;

            set
            {
                if (value >= _MAX_TIER)
                    throw new ArgumentException("Course angle out of range");
                else
                {

                    this._tier = value;
                    this.OnPropertyChanged("Tier");
                }
            }
        }

        [Category("ARD")]
        [DisplayName("State")]
        [Description("ARD state"),  Browsable(false)]
        public bool State
        {
            get ;set;
         
        }


        [Category("ARD")]
        [DisplayName("Lower tier angle")]
        [Description("Lower tier ARD angle"), ReadOnly(true), Browsable(false)]
        public List<float> TierAngle
        {
            get => this._tierAngle;

            set
            {
                foreach (var t in value)
                {
                    if (t >= 0 && t <= 360)
                    {
                        throw new ArgumentException("TierAngle angle out of range");
                    }

                    this._tierAngle = value;
                    this.OnPropertyChanged("TierAngle");
                }
            }
        }

        [Category("ARD")]
        [DisplayName("Color")]
        [Description("ARD color on chart"), ReadOnly(true), Browsable(false) ]
        public Brush DColor { get; set; }



        [Category("Orientation")]
        [DisplayName("Correction")]
        [Description("Correction angle"), Browsable(false)]
        public float Correct
        {
            get => this._сorrect;

            set
            {
                if (value >= 0 && value <= 360)
                {
                    this._сorrect = value;
                    this.CountDirect();

                    this.OnPropertyChanged("Correct");
                }

                else
                    throw new ArgumentException("Correct angle out of range");

            }
        }


        [Category("Orientation")]
        [DisplayName("Course angle")]
        [Description("Course angle")]
        public float Course
        {
            get => this._сourse;

            set
            {
                if (value >= 0 && value <= 360)
                {
                    this._сourse = value;
                    this.CountDirect();

                    this.OnPropertyChanged();
                }

                else
                    throw new ArgumentException("Course angle out of range");

            }
        }

        [Category("Orientation")]
        [DisplayName("ARD angle")]
        [Description("ARD angle"), ReadOnly(true), Browsable(true)]
        public float Angle
        {
            get => this._angle;

            set
            {
                if (value >= 0 && value <= 360)
                {
                    this._angle = (float)Math.Round(value, 1);
                    this.CountDirect();

                    this.OnPropertyChanged("Angle");
                }
                else
                    throw new ArgumentException("Angle out of range");
            }
        }

        [Category("Orientation")]
        [DisplayName("Elevation")]
        [Description("ARD elevation"),ReadOnly(true), Browsable(false)]
        public float Elevation
        {
            get => this._elevation;

            private set
            {
                if (value >= 0 && value <= 360)
                {
                    this._elevation = value;

                    this.OnPropertyChanged("Elevation");
                }
                else
                    throw new ArgumentException("Elevation out of range");
            }
        }

        [Category("Orientation")]
        [DisplayName("Azimuth")]
        [Description("ARD azimuth"), ReadOnly(true), Browsable(true)]
        public float Direct
        {
            get => this._direct;

            private set
            {
                this._direct = (float)Math.Round(value, 1);
                this.OnPropertyChanged("Direct");

                this.UpdateDirect(this, new EventArgs());
            }
        }

        [Category("Orientation")]
        [DisplayName("Turn")]
        [Description("Direction Set BRD "), Browsable(false)]
        public float DirectionSet { get; set; }


        


        #endregion


        public CompassModel( )
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
            this.CountDirect();

        }
            

        #region Method

        public bool SetDirect(float Azimuth, float Elevat)
        {
            try
            {
                var fDif = Azimuth - this.Direct;
                var resAngle = this.Angle + fDif;
                while ((resAngle) < 0)
                    resAngle += 360;

                while ((resAngle) > 360)
                    resAngle -= 360;

                return this.SetAngle(resAngle, Elevat);


            }
            catch
            {
                return false;
            }

        }

        private void CountDirect()
        {
            try
            {
                this._direct = (this._сorrect + this._сourse + this._angle);
                var i = 1;
                while (i < this._tier)
                {
                    this._direct += this._tierAngle[i-1];
                    i--;
                }

                while (this._direct > 360)
                {
                    this._direct -= 360;
                }

                this.Direct = this._direct;

                //OnUpdate?.Invoke(this, new UpdEventArgs(Angle, Elevation, Direct));
            }
            catch { }
        }

        protected override void GetAngleEvent(object sender, AngleEventArgs e)
        {
            this.Angle = e._angle;
            this.Elevation = e._elevation;
        }
        #endregion


        //#region Event

        //public event EventHandler<UpdEventArgs> OnUpdate;
        

        //#endregion




        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
           
        }


        public static event EventHandler OnUpdateDirect;

        public void UpdateDirect(object sender,EventArgs e)
        {
            OnUpdateDirect?.Invoke(this, e);
        }


        #region Test

        ////private DispatcherTimer _timer = null;


        //public void StopMethod()
        //{

        //    this._timer.Stop();

        //}

        //public void StartMethod()
        //{
        //    this._timer = new DispatcherTimer();
        //    this._timer.Tick += new EventHandler(this._dispatcherTimer_Tick);
        //    this._timer.Interval = new TimeSpan(0, 0, 0, 0, 10);
        //    this._timer.Start();
            

        //}

        //private void _dispatcherTimer_Tick(object sender, EventArgs e)
        //{
        //    var def = this.Direct - this.DirectionSet;
        //    if (def <0)
        //        def = this.DirectionSet - this.Direct ;
        //    if (def > 2)
        //    {
        //        this.Angle++;
        //    }
        //    else
        //        this.StopMethod();
        //}
        #endregion


    }
}
