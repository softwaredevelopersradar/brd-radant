﻿
using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CRadantBRD
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class RadantCtrl : UserControl
    {

        public List<Radant> curRadant = new List<Radant>();

        List<AnnotationPolar> vectorRadant = new List<AnnotationPolar>();

        public static readonly double MaxAmplitude = 250.0;


        public RadantCtrl()
        {
            InitializeComponent();
        }


        public void AddRadant(List<Radant> radant)
        {
            foreach (Radant obj in radant)
            {
                curRadant.Add(obj);

                var vector = new AnnotationPolar();

                var btn = new Button();

                var bord = new System.Windows.Controls.Border();
                vectorRadant.Add(vector);

                AddItemBRD();

                AddVectorBRD();

                gridBRD.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(0.5, GridUnitType.Star) });

                gridBRD.Children.Add(bord);
                Grid.SetRow(bord, gridBRD.RowDefinitions.Count - 1);
                //  obj.OpenPort();


                void AddItemBRD()
                {
                    bord.Background = new BrushConverter().ConvertFromString("#FF494848") as SolidColorBrush;

                    bord.Height = 35;
                    //bord.BorderBrush = Brushes.White;
                    bord.BorderBrush = obj.DColor;
                    bord.BorderThickness = new Thickness(1, 1, 1, 1);
                    bord.Margin = new Thickness(5, 5, 5, 5);
                    bord.CornerRadius = new CornerRadius(2, 2, 2, 2);

                    bord.Child = btn;


                    btn.Margin = new Thickness(0, 0, 0, 0);
                    btn.Background = new BrushConverter().ConvertFromString("#FF424141") as SolidColorBrush;

                    btn.HorizontalAlignment = HorizontalAlignment.Stretch;
                    btn.Content = obj.Name + "  " + obj.Direct.ToString();
                    //btn.Foreground = obj.DColor;
                    btn.Foreground = Brushes.White;


                    btn.Tag = radant.IndexOf(obj);

                    btn.Click += (source, e) =>
                    {
                        PropertyGrid2.SelectedObject = curRadant[(int)(btn.Tag)];

                        foreach (AnnotationPolar objPolar in vectorRadant)
                        {
                            objPolar.ArrowLineStyle.Width = 2;
                        }

                        IEnumerable<System.Windows.Controls.Border> collection = gridBRD.Children.OfType<System.Windows.Controls.Border>();

                        foreach (System.Windows.Controls.Border objBorder in collection)
                        {
                            objBorder.BorderThickness = new Thickness(1, 1, 1, 1);
                        }

                        List<System.Windows.Controls.Border> list = collection.ToList();
                        list[(int)(btn.Tag)].BorderThickness = new Thickness(3, 3, 3, 3);

                        vectorRadant[(int)(btn.Tag)].ArrowLineStyle.Width = 5;


                    };
                }
                void AddVectorBRD()
                {
                    vector.Style = AnnotationStyle.Arrow;

                    //Location is where the vector starts from
                    vector.LocationCoordinateSystem = CoordinateSystem.AxisValues;
                    vector.LocationAxisValues.Angle = 0;
                    vector.LocationAxisValues.Amplitude = -15;
                    //Target is where the vector points to. All vectors are equal length in this example. 
                    vector.TargetAxisValues.Amplitude = MaxAmplitude;
                    vector.ArrowLineStyle.Width = 3;
                    vector.MouseInteraction = false;
                    vector.ArrowStyleBegin = ArrowStyle.None;
                    vector.ArrowStyleEnd = ArrowStyle.Arrow;

                    vector.ArrowLineStyle.Color = ((SolidColorBrush)obj.DColor).Color;

                    vector.ArrowLineStyle.Width = 2;
                    vector.ArrowLineStyle.Color = ((SolidColorBrush)obj.DColor).Color;

                    vector.TargetAxisValues.Angle = obj.Direct;
                    vector.TextStyle.Visible = false;

                }


                _chart.ViewPolar.Annotations.Add(vectorRadant[vectorRadant.Count - 1]);
            }

            try
            {
                PropertyGrid2.SelectedObject = curRadant[0];
                vectorRadant[0].ArrowLineStyle.Width = 5;
                IEnumerable<System.Windows.Controls.Border> collection = gridBRD.Children.OfType<System.Windows.Controls.Border>();


                List<System.Windows.Controls.Border> list = collection.ToList();
                list[0].BorderThickness = new Thickness(3, 3, 3, 3);
            }

            catch
            { }
        }
    }
}
