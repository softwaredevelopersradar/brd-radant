﻿using CompassControl.Properties;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Data;

namespace CompassControl
{
    public class NameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {/*.CurrentUICulture */
                //Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");
                //var sRole = new ResourceManager(typeof(Resources));
                //var sRole = new ResourceManager("CompassControl.Properties.Resources", Assembly.GetAssembly(typeof(Resources)));
                var sRole = new ResourceManager("CompassControl.Properties.Resources", Assembly.GetExecutingAssembly());
                //var sRole = Resources.ResourceManager;
                var resourceDisplayName = sRole.GetString((string)value, Thread.CurrentThread.CurrentCulture);

                return resourceDisplayName;
            }
            catch(Exception ex){ return ""; }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        //public static string RoleSign(object role)
        //{
        //    string sRole = string.Empty;

        //    switch (role)
        //    {
        //        case StationRole.Own:
        //            sRole = "*";
        //            break;

        //        case StationRole.Linked:
        //            sRole = "→";
        //            break;

        //        case StationRole.Complex:
        //            sRole = string.Empty;
        //            break;

        //        default:
        //            sRole = string.Empty;
        //            break;
        //    }

        //    return sRole;
        //}
    }
}
