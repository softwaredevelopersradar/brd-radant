﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using System.Windows;

using System.Windows.Input;


namespace CompassControl
{
    using CompassModelLibrary;

    public class RadantViewModel : DependencyObject, INotifyPropertyChanged
    {
        public ObservableCollection<CompassModel> Radants
        {
            get => radants;
            set
            {
                radants = value;
                if (radants.Count > 0)
                {
                    SelectedRadant = radants[0];
                }
            }
        }

        private ObservableCollection<CompassModel> radants;
        private CompassModel _selectedRadant = new CompassModel();
        private ICommand _getRadantCommand;
        private ICommand _setRadantCommand;
        private ICommand _stopRadantCommand;
        private ICommand _saveParamCommand;
        private ICommand _cancelRadantCommand;
        private Visibility leftGridVisibility = Visibility.Collapsed;

        public Visibility LeftGridVisibility
        {
            get => this.leftGridVisibility;
            set
            {
                if (value == this.leftGridVisibility) return;
                this.leftGridVisibility = value;
                this.OnPropertyChanged();
            }
        }

        public CompassModel SelectedRadant
        {
            get => _selectedRadant;
            set { _selectedRadant = value;
                OnUpdateSelectedRadant(this, new CompassModel());
                //OnPropertyChanged();
            }
        }

        

        public RadantViewModel()
        {
            Radants = new ObservableCollection<CompassModel>();
        }




        #region Command
        public ICommand SetRadantCommand
        {
            get
            {
                if (_setRadantCommand == null)
                {
                    _setRadantCommand = new RelayCommand(
                        param => SetRadant(),
                        param => (SelectedRadant != null)
                    );
                }
                return _setRadantCommand;
            }
        }

        public ICommand GetRadantCommand
        {
            get
            {
                if (_getRadantCommand == null)
                {
                    _getRadantCommand = new RelayCommand(
                        param => GetRadant(),
                        param => (SelectedRadant != null)
                    );
                }
                return _getRadantCommand;
            }
        }

        public ICommand StopRadantCommand
        {
            get
            {
                if (_stopRadantCommand == null)
                {
                    _stopRadantCommand = new RelayCommand(
                        param => StopRadant(),
                        param => (SelectedRadant != null)
                    );
                }
                return _stopRadantCommand;
            }
        }

        public ICommand SaveParamCommand
        {
            get
            {
                if (_saveParamCommand == null)
                {
                    _saveParamCommand = new RelayCommand(
                        param => SaveParam(),
                        param => (SelectedRadant != null)
                    );
                }
                return _saveParamCommand;
            }
        }

        public ICommand CancelParamCommand
        {
            get
            {
                if (_cancelRadantCommand == null)
                {
                    _cancelRadantCommand = new RelayCommand(
                        param => CancelParam(),
                        param => (SelectedRadant != null)
                    );
                }
                return _cancelRadantCommand;
            }
        }

        private void GetRadant()
        {
            // You should get the product from the database
            // but for now we'll just return a new object

            SelectedRadant.Device.GetAngle();
        }

        private void SetRadant()
        {
            // You would implement your Product save here
            SelectedRadant.SetDirect(SelectedRadant.DirectionSet, SelectedRadant.Elevation);
        }

        private void StopRadant()
        {
            // You would implement your Product save here
            SelectedRadant.Device.Stop();
        }

        private void SaveParam()
        {
            OnUpdateSelectedRadant(this, SelectedRadant);
        }

        private void CancelParam()
        {
            // You would implement your Product save here
            ;
        }


        public void AddRadant(List<CompassModel> compassModels)
        {
            foreach (CompassModel element in compassModels)
                Radants.Add(element);

            try
            {
                SelectedRadant = Radants[0];
            }
            catch { }
            
        }
        public void DeleteRadant()
        {
            if (SelectedRadant != null)
            {
                Radants.Remove(SelectedRadant);
            }
        }


        #endregion

       
        public EventHandler<CompassModel> OnUpdateSelectedRadant { get; set; } = (object sender, CompassModel arg) => {};

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }


    }



}
