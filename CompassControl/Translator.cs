﻿using CompassControl.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

namespace CompassControl
{
    public class Translator
    {
        public static void ChangeLanguagePropertyGrid(Languages language, System.Windows.Controls.WpfPropertyGrid.PropertyGrid propertyGrid)
        {
            LoadDictionary(language);

            foreach (var prop in propertyGrid.Properties)
            {
                SetNamePropery(prop.Name, propertyGrid);
            }

            foreach (var category in propertyGrid.Categories)
            {
                SetNameCategory(category.Name, propertyGrid);
            }
        }

        private static void SetNamePropery(string nameProperty, System.Windows.Controls.WpfPropertyGrid.PropertyGrid propertyGrid)
        {
            try
            {
                propertyGrid.Properties[nameProperty].DisplayName = TranslateDic[nameProperty];
            }
            catch (Exception) { }
        }

        private static void SetNameCategory(string nameCategory, System.Windows.Controls.WpfPropertyGrid.PropertyGrid propertyGrid)
        {
            try
            {
                propertyGrid.Categories.First(t => t.Name == nameCategory).HeaderCategoryName = TranslateDic[nameCategory];
            }
            catch (Exception) { }
        }

        static Dictionary<string, string> TranslateDic;
        public static void LoadDictionary(Languages language)
        {
            XmlDocument xDoc = new XmlDocument();


            var path = Directory.GetCurrentDirectory() + "\\Languages\\TranslatorLeftControls\\Translator.xml";
            if (File.Exists(Directory.GetCurrentDirectory() + "\\Languages\\TranslatorLeftControls\\Translator.xml"))
                xDoc.Load(Directory.GetCurrentDirectory() + "\\Languages\\TranslatorLeftControls\\Translator.xml");
            

            TranslateDic = new Dictionary<string, string>();

            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode x2Node in xRoot.ChildNodes)
            {
                if (x2Node.NodeType == XmlNodeType.Comment)
                    continue;

                // получаем атрибут ID
                if (x2Node.Attributes.Count > 0)
                {
                    XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                    if (attr != null)
                    {
                        foreach (XmlNode childnode in x2Node.ChildNodes)
                        {
                            // если узел - language
                            if (childnode.Name == language.ToString())
                            {
                                if (!TranslateDic.ContainsKey(attr.Value))
                                    TranslateDic.Add(attr.Value, childnode.InnerText);
                            }
                        }
                    }
                }
            }

            
        }
    }


    
}
