﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace CompassControl.Converters
{
    [ValueConversion(typeof(Visibility), typeof(GridLength))]
    public class VisibilityToGridRowWidthConverter : IValueConverter
    {
        #region Implementation of IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter is int size)
            {
                return value != null && (Visibility)value == Visibility.Visible
                    ? new GridLength(size, GridUnitType.Pixel)
                    : new GridLength(0);
            }

            return value != null && (Visibility)value == Visibility.Visible
                ? new GridLength(1, GridUnitType.Star)
                : new GridLength(0);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }

        #endregion
    }
}