﻿

using CRadantBRD.Events;
using CRadantBRD.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;


namespace CRadantBRD
{

   
    public class Radant : ComBRD, IBRD
    {
        #region Const

        private const byte _MAX_TIER = 5;

        #endregion

        #region Field

        private float _angle;
        private float _elevation;

        private float _сorrect;
        private float _сourse;

        private byte _tier;
        private List<float> _tierAngle;

        #endregion
        

        #region Property

        [Category("Base")]
        [DisplayName("Name")]
        [Description("Name BRD"), ReadOnly(true), Browsable(true)]
        public string Name { get; set; }

        [Category("Base")]
        [DisplayName("Color")]
        [Description("Color limb"), ReadOnly(true), Browsable(false) ]
        public Brush DColor { get; set; }


        [Category("BRD")]
        [DisplayName("Angle")]
        [Description("Angle BRD"), ReadOnly(true), Browsable(true)]
        public float Angle
        {
            get => _angle;

            private set
            {
                if (value >= 0 && value <= 360)
                {
                    _angle = value;
                    CountDirect();
                }
                else
                    throw new ArgumentException("Angle out of range");
            }
        }

        [Category("BRD")]
        [DisplayName("Elevation")]
        [Description("Elevation BRD"),ReadOnly(true), Browsable(true)]
        public float Elevation
        {
            get => _elevation;

            private set
            {
                if (value >= 0 && value <= 360)
                {
                    _elevation = value;
                }
                else
                    throw new ArgumentException("Elevation out of range");
            }
        }

        [Category("Base")]
        [DisplayName("Direct")]
        [Description("Direction BRD "), ReadOnly(true), Browsable(true)]
        public float Direct { get; private set; }


        [Category("Base")]
        [DisplayName("Correct")]
        [Description("Correction angle")]
        public float Correct
        {
            get => _сorrect;

            set
            {
                if (value >= 0 && value <= 360)
                {
                    _сorrect = value;
                    CountDirect();
                }

                else
                    throw new ArgumentException("Correct angle out of range");

            }
        }

        [Category("Base")]
        [DisplayName("Course")]
        [Description("Course angle")]
        public float Course
        {
            get => _сourse;

            set
            {
                if (value >= 0 && value <= 360)
                {
                    _сourse = value;
                    CountDirect();
                }

                else
                    throw new ArgumentException("Course angle out of range");

            }
        }

        [Category("BRD")]
        [DisplayName("Tier")]
        [Description("Number of tier BRD")]
        public byte Tier
        {
            get => _tier;

            set
            {
                if (value >= _MAX_TIER)
                    throw new ArgumentException("Course angle out of range");
                else
                    _tier = value;
            }
        }

        [Category("BRD")]
        [DisplayName("TierAngle")]
        [Description("Tier angles the lowerest BRD"), ReadOnly(true), Browsable(false)]
        public List<float> TierAngle
        {
            get => _tierAngle;

            set
            {
                foreach (var t in value)
                    if (t >= 0 && t <= 360)
                    {
                        throw new ArgumentException("TierAngle angle out of range");
                    }

                _tierAngle = value;
            }
        }

        #endregion


        public Radant( )
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
            CountDirect();

        }
            

        #region Method

        public bool SetDirect(float Azimuth, float Elevat)
        {         
            try
            {
                var resAngle = Azimuth - Direct;
                while ((resAngle) < 0)
                    resAngle += 360;

                return SetAngle(resAngle, Elevat);
            }
            catch
            {
                return false;
            }

        }

        private void CountDirect()
        {
            try
            {
                var _direct = (_сorrect + _сourse + _angle);
                var i = 0;
                while (i < _tier)
                {
                    _direct += _tierAngle[i];
                    i--;
                }

                while (_direct > 360)
                {
                    _direct -= 360;
                }

                Direct = _direct;

                OnUpdate?.Invoke(this, new UpdEventArgs(Angle, Elevation, Direct));
            }
            catch { }
        }

        protected override void GetAngleEvent(object sender, AngleEventArgs e)
        {
            Angle = e._angle;
            Elevation = e._elevation;
        }
        #endregion


        #region Event

        public event EventHandler<UpdEventArgs> OnUpdate;       
          
        #endregion


    }
}
