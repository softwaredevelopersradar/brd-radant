﻿namespace CompassControl.Enums
{
    public enum ColorDirect
    {
        Blue = 0x0000FF,
        Lime = 0x00FF00,
        Indigo = 0x4B0082,
        Maroon = 0x800000,
        OrangeRed = 0xFF4500,
        Olive = 0x808000

    }
}
