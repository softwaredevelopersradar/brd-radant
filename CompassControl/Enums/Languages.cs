﻿namespace CompassControl.Enums
{
    public enum Languages
    {
        RU = 0,
        EN = 1,
        AZ = 2
    }
}
