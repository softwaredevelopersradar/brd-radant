﻿
using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.Annotations;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using CompassControl.Enums;

namespace CompassControl
{
    using CompassControl.Properties;
    using CompassModelLibrary;
    using System.Globalization;
    using System.IO;
    using System.Resources;
    using System.Threading;

    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class RadantView : UserControl
    {
        
        private List<AnnotationPolar> vectorRadant = new List<AnnotationPolar>();

        private static readonly double MaxAmplitude = 250.0;
        
        private RadantViewModel radantViewModel;

        public ObservableCollection<CompassModel> Radants
        {
            get => radantViewModel.Radants;
            set { radantViewModel.AddRadant(new List<CompassModel>(value)); }           
        }

      

        public RadantView()
        {
            var deploymentKey = "lgCAALq+gYUUUNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA5LTE5I1JldmlzaW9uPTACgD8RLUj4MtJqUZJYP08cAT2iWwf3J9OtBpdL7i2N+0kh7SWkWVA97OxGhM4wObsk67coGddfPr0up6PC3C0KPwwMCiXxkTBWdZ08iYj+WZzzt0Nh0WCA1IHun718ZKUQZyIfbWo+Zm/ye5a/SYJRwoenYVg95HKI3lUD+tAs9E5lNkeIgHiWrxUpQCFd+lN3d6SVnmaaRwMoLdT6iZF8bbI9drqJKFlcQX2RmV5CHt9ABh2AS4G8AbJMUHJrk4dxbyYxZwINYnlhMJPLtitabW+iK+ZQel62jiAm7jPlCGuWUEX34UUVZkOov5jUOIfYuIlgeDLbVNrcdWXOyy34b/+/D5WBCfBrbbINSOFtti+asDiZzEK6nbPL4FU90A14EOFoY68fsAkM3mdae2V0Kn42zZ0imxz84lfwsYfgjDO7MLwKmeU8YSJQJgRTL4A7bJl72NmDGlBnoPCOvDiTOe3xzEKwBle0yNbQfvvOJ47mwibGxo0gxv/o78RSq0SaaMs=";
            LightningChartUltimate.SetDeploymentKey(deploymentKey);
            InitializeComponent();
            //SetResourcelanguage();
            radantViewModel = new RadantViewModel();
            radantViewModel.OnUpdateSelectedRadant += UpdateSelected;
            radantViewModel.Radants.CollectionChanged += ChangedEvent;
            radantViewModel.SelectedRadant.PropertyChanged += (sender, args) => ChangedEvent(sender,null);
            CompassModel.OnUpdateDirect += UpdateDirect;
            DataContext = radantViewModel;


        }


        public static readonly DependencyProperty IsAdminProperty = DependencyProperty.Register("IsAdmin", typeof(bool), typeof(RadantView), new FrameworkPropertyMetadata(true));
        public bool IsAdmin
        {
            get { return (bool)GetValue(IsAdminProperty); }
            set { SetValue(IsAdminProperty, value); }
        }


        private void UpdateViewRadant()
        {
            vectorRadant.Clear();
            _chart.ViewPolar.Annotations.Clear();


            foreach (CompassModel obj in radantViewModel.Radants)
            {
                var vector = new AnnotationPolar();

                AddVectorBRD();

                vectorRadant.Add(vector);

                _chart.ViewPolar.Annotations.Add(vectorRadant[vectorRadant.Count - 1]);

                void AddVectorBRD()
                {
                    vector.Style = AnnotationStyle.Arrow;

                    //Location is where the vector starts from
                    vector.LocationCoordinateSystem = CoordinateSystem.AxisValues;
                    vector.LocationAxisValues.Angle = 0;
                    vector.LocationAxisValues.Amplitude = -15;
                    //Target is where the vector points to. All vectors are equal length in this example. 
                    vector.TargetAxisValues.Amplitude = MaxAmplitude;
                    vector.ArrowLineStyle.Width = 3;
                    vector.MouseInteraction = false;
                    vector.ArrowStyleBegin = ArrowStyle.None;
                    vector.ArrowStyleEnd = ArrowStyle.Arrow;

                    vector.ArrowLineStyle.Color = (obj.DColor as SolidColorBrush).Color;
                    vector.TargetAxisValues.Angle = obj.Direct;
                    vector.TextStyle.Visible = false;

                }
            }

        }


        private void UpdateSelected(object sender, CompassModel e)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                PropertyGrid2.SelectedObject = radantViewModel.SelectedRadant;
                var i = radantViewModel.Radants.IndexOf(radantViewModel.SelectedRadant);
                ShowArrow(i, radantViewModel.SelectedRadant);
            }));

            SetLanguage(Lang);
        }

        private void ChangedEvent(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            UpdateViewRadant();
        }

        private void UpdateDirect(object sender, EventArgs e)
        {
            
            var i = radantViewModel.Radants.IndexOf(radantViewModel.SelectedRadant);
            ShowArrow(i, radantViewModel.SelectedRadant);

        }



        private void ShowArrow(int ind, CompassModel radantModel)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {

                if (ind > -1)
                {
                    foreach (AnnotationPolar obj in vectorRadant)
                        obj.ArrowLineStyle.Width = 3;


                    vectorRadant.ElementAt(ind).TargetAxisValues.Angle = radantModel.Direct;
                    vectorRadant.ElementAt(ind).ArrowLineStyle.Width = 5;
                }


            }));
        }

        #region Method


        public void UpdateCourseAngle(float course)
        {
            if (Radants.Count == 0)
            {
                return;
            }

            foreach (var compass in Radants)
            {
                switch (compass.Name)
                {
                    case "RRS_Lincked":
                        compass.Course = course;
                        break;
                    case "RRS_PC":
                        compass.Course = course;
                        break;
                    default:
                    {
                        if (course + 270 > 360)
                        {
                            compass.Course = course + 270 - 360;
                        }
                        else
                        {
                            compass.Course = course + 270;
                        }
                        break;
                    }
                       
                }
            }
        }

        #endregion

        Languages Lang = Languages.RU;
        
        //private void SetResourcelanguage()
        //{
        //    ResourceDictionary dict = new ResourceDictionary();
        //    try
        //    {
        //        switch (Lang)
        //        {
        //            case Languages.EN:
        //                dict.Source = new Uri( "/CRadantBRD;component/DLanguages/StringResource.EN.xaml",
        //                              UriKind.Relative);
        //                break;
        //            case Languages.RU:
        //                dict.Source = new Uri("/CRadantBRD;component/DLanguages/StringResource.xaml",
        //                                   UriKind.Relative);
        //                break;
        //            default:
        //                dict.Source = new Uri("/CRadantBRD;component/DLanguages/StringResource.xaml",
        //                                  UriKind.Relative);
        //                break;
        //        }

        //        this.Resources.MergedDictionaries.Add(dict);
        //    }
        //    catch (Exception ex)
        //    { }
        //}

        public void SetLanguage(Languages language)
        {
            Lang = language;
            LoadTranslator(language);
            Translator.ChangeLanguagePropertyGrid(language, PropertyGrid2);
        }

        private void LoadTranslator(Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case Languages.EN:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorLeftControls/TranslatorLeftControls.EN.xaml",
                                      UriKind.Relative);
                        Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
                        break;
                    case Languages.RU:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorLeftControls/TranslatorLeftControls.RU.xaml",
                                      UriKind.Relative);
                        Thread.CurrentThread.CurrentUICulture = new CultureInfo("ru-RU");
                        break;
                    default:
                        dict.Source = new Uri("/GrozaFuture;component/Languages/TranslatorLeftControls/TranslatorLeftControls.RU.xaml",
                                      UriKind.Relative);
                        Thread.CurrentThread.CurrentUICulture = new CultureInfo("ru-RU");
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void _chart_MouseClick(object sender, MouseButtonEventArgs e)
        {
            
        }

        private void _chart_MouseMove(object sender, MouseEventArgs e)
        {
            Point p = e.GetPosition(this);
            double x = p.X;
            double y = p.Y;
        }


        
    }
}
