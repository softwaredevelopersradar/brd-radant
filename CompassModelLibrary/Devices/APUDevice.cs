﻿using ModelsTablesDBLib;

namespace CompassModelLibrary.Devices
{
    using System;
    using System.Collections.Generic;
    using ARD_DLL;
    using Com;
    using Enums;
    using Interface;

    public class APUDevice : IComBRD
    {
        private ComARD_APU ard = new ComARD_APU();

        private ComParam comSettings;

        public event EventHandler<ComParam> OnOpenPort = delegate { };
        public event EventHandler<ComParam> OnClosePort = delegate { };
        public event EventHandler<ByteEventArgs> OnReadByte = delegate { };
        public event EventHandler<ByteEventArgs> OnWriteByte = delegate { };
        public event EventHandler OnStop = delegate { };
        public event EventHandler<AngleEventArgs> OnGetAngle = delegate { };
        public event EventHandler OnSetAngle = delegate { };
        public event EventHandler OnError = delegate { };

        public APUDevice(ComParam settings)
        {
            InitiateComArdEvents();
            this.comSettings = settings; //todo why is no such params in ARDConnection?
            
            this.properties = new ARDConnection()
            {
                Address = 1,
                ComPort = settings.PortName,
                Device = DeviceRSD.SSTU,
                Level = 1,
                PortSpeed = settings.BaudRate,
                State = true,
                Type = TypeRSD.RSD_20
            };
        }

        public APUDevice(ARDConnection connection)
        {
            InitiateComArdEvents();
            this.properties = connection; //todo delete?
            this.comSettings = new ComParam(
                connection.ComPort, 
                connection.PortSpeed,
                System.IO.Ports.Parity.None,
                8,
                System.IO.Ports.StopBits.One);
        }

        public ARDConnection properties { get; set; }
        public bool State { get; private set; }
        public void OpenPort()
        {
            ard.OpenPort(comSettings.PortName, comSettings.BaudRate, comSettings.Parity, comSettings.DataBits, comSettings.StopBits);
            State = true;
            this.OnOpenPort?.Invoke(this, comSettings);
        }

        public void AddData(List<byte> data)
        {
            
        }

        public void ClosePort()
        {
            ard.ClosePort();
            State = false;
            this.OnClosePort?.Invoke(this, comSettings);
        }


        public bool SetAngle(float angle, float elevation)
        {
            try
            {
                ard.SendSetAngle((byte)properties.Address, 1, (short)angle, 0);
                return true;
            }
            catch
            {
                Stop();
                return false;
            }

        }

        public bool SetDirect(float direction, float elevation)
        {
            try
            {
                ard.SendFreeRotate((byte) properties.Address, (byte) direction, (short) elevation);
                return true;
            }
            catch
            {
                Stop();
                return false;
            }
        }

        public bool Rotate(float direction)
        {
            try
            {
                ard.SendFreeRotate((byte)properties.Address, (byte)direction, 100);
                return true;
            }
            catch (Exception e)
            {
                Stop();
                return false;
            }
        }

        public bool GetAngle()
        {
            try
            {
                ard.SendGetAngle((byte) properties.Address);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Stop()
        {
            try
            {
                ard.SendStop((byte)properties.Address, 1);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        private void InitiateComArdEvents()
        {
            ard.OnWriteByte += bByte => OnWriteByte?.Invoke(this, new ByteEventArgs(new List<byte>(bByte)));
            ard.OnSendCmd += (part, values) =>
            {
                OnWriteByte?.Invoke(this, null);
            };
            ard.OnConnectPort += () =>
            {
                OnOpenPort?.Invoke(this, comSettings);
                this.State = true;
            };
            ard.OnStopCmd += (address, error) => OnStop?.Invoke(this, null);
            ard.OnDisconnectPort += () => OnClosePort?.Invoke(this, comSettings);
            ard.OnSetAngleCmd += (address, error) =>
            {
                OnSetAngle?.Invoke(this, null);
                this.GetAngle();
            };
            ard.OnGetAngleCmd += (sender, address, angle, motor, error) =>
            {
                OnGetAngle?.Invoke(this, new AngleEventArgs(angle, 0));
            };
            ard.OnReadByte += bByte => OnReadByte?.Invoke(this, new ByteEventArgs(new List<byte>(bByte)));
        }
    }
}