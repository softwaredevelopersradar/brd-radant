﻿using ModelsTablesDBLib;

namespace CompassModelLibrary.Interface
{
    using System;
    using Enums;

    public interface IComBRD : IComBase
    {
        event EventHandler OnStop;
        event EventHandler<AngleEventArgs> OnGetAngle;
        event EventHandler OnSetAngle;
        event EventHandler OnError;

        ARDConnection properties { get; set; }

        bool SetAngle(float angle, float elevation);
        bool Rotate(float direction);
        bool GetAngle();
        bool Stop();
    }
}