﻿namespace CompassModelLibrary.Interface
{
    using System;
    using System.Collections.Generic;
    using Com;
    using Enums;

    public interface IComBase
    {
        bool State { get;}
        void OpenPort();
        void AddData(List<byte> data);
        void ClosePort();
      
        event EventHandler<ComParam> OnOpenPort;
        event EventHandler<ComParam> OnClosePort;
        event EventHandler<ByteEventArgs> OnReadByte;
        event EventHandler<ByteEventArgs> OnWriteByte;
    }
}
