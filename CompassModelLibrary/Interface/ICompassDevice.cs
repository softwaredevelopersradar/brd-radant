﻿namespace CompassModelLibrary.Interface
{
    using System.ComponentModel;
    using ModelsTablesDBLib;

    public interface ICompassDevice : IBRD
    {
        IComBRD Device { get; set; }

        bool SetDirect(float azimuth, float elevation);
        void CountDirect();
    }
}