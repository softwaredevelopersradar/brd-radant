﻿namespace CompassModelLibrary.Interface
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Media;

    public interface IBRD : INotifyPropertyChanged
    {
        [Category("Orientation")]
        [DisplayName("ARD angle")]
        [Description("ARD angle"), ReadOnly(true), Browsable(true)]
        float Angle { get;}

        [Category("Orientation")]
        [DisplayName("Azimuth")]
        [Description("ARD azimuth"), ReadOnly(true), Browsable(true)]
        float Direct { get; }

        [Category("Orientation")]
        [DisplayName("Elevation")]
        [Description("ARD elevation"), ReadOnly(true), Browsable(false)]
        float Elevation { get; }

        [Category("Orientation")]
        [DisplayName("Turn")]
        [Description("Direction Set BRD "), Browsable(false)]
        float DirectionSet { get; set; }


        [Category("ARD")]
        [DisplayName("Name")]
        [Description("ARD name"), ReadOnly(true), Browsable(true)]
        string Name { get; }

        [Category("ARD")]
        [DisplayName("Color")]
        [Description("ARD color on chart"), ReadOnly(true), Browsable(false)]
        Brush DColor { get; }

        [Category("Orientation")]
        [DisplayName("Correction")]
        [Description("Correction angle"), Browsable(false)]
        float Correct { get;  set; }


        [Category("Orientation")]
        [DisplayName("Course angle")]
        [Description("Course angle")]
        float Course { get;  set; }


        [Category("ARD")]
        [DisplayName("Tier")]
        [Description("ARD tier"), ReadOnly(true)]
        byte Tier { get;  set; }

        [Category("ARD")]
        [DisplayName("Lower tier angle")]
        [Description("Lower tier ARD angle"), ReadOnly(true), Browsable(false)]
        List<float> TierAngle { get;  set; }

    }
}
