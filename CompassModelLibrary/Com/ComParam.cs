﻿namespace CompassModelLibrary.Com
{
    using System;
    using System.IO.Ports;

    public class ComParam : EventArgs
    {
        public string PortName;
        public Int32 BaudRate;
        public Parity Parity;
        public Int32 DataBits;
        public StopBits StopBits;


        public ComParam()
        { }

        public ComParam(string portName, Int32 baudRate, Parity parity, Int32 dataBits, StopBits stopBits)
        {
            this.PortName = portName;
            this.BaudRate = baudRate;
            this.Parity = parity;
            this.DataBits = dataBits;
            this.StopBits = stopBits;
        }
    }
}
