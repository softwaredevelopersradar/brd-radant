﻿namespace CompassModelLibrary.Com
{
    using System;
    using System.Collections.Generic;
    using System.IO.Ports;
    using System.Linq;
    using System.Threading;
    using Enums;
    using Interface;

    public abstract class ComBase
    {
        private SerialPort _port;
        private bool _state;
       
        private Thread _thrRead;
        private const int _sizeRead = 20000;

        public ComBase()
        {


        }

        #region Properties
        public bool State {
            get => this._state;

            private set
            {
                this._state = value;
            }
        }

        #endregion

        #region Events

        public event EventHandler<ComParam> OnOpenPort;
        public event EventHandler<ComParam> OnClosePort;
        public event EventHandler<ByteEventArgs> OnReadByte;
        public event EventHandler<ByteEventArgs> OnWriteByte;

        protected virtual void OpenPortEvent(object sender, ComParam e)
        {
            this.OnOpenPort?.Invoke(this, e);
        }
        protected virtual void ClosePortEvent(object sender, ComParam e)
        {
            this.OnClosePort?.Invoke(this, e);
        }
        protected virtual void ReadByteEvent(object sender, ByteEventArgs e)
        {
            this.OnReadByte?.Invoke(this, e);
        }
        protected virtual void WriteByteEvent(object sender, ByteEventArgs e)
        {
            this.OnWriteByte?.Invoke(this, e);
        }

        #endregion


        public void Test()
        {
          
            List<byte> listTest = new List<byte>() { 1, 2, 3, 45 };
            this.OnReadByte(this, new ByteEventArgs(listTest));
        }

        // Open COM port
        public void OpenPort(ComParam comParam)
        {
            // Open COM port

            if (this._port == null)
                this._port = new SerialPort();

            // if port is open
            if (this._port.IsOpen)

                // close it
                this.ClosePort();

            // try to open 
            try
            {
                // set parameters of port
                this._port.PortName = comParam.PortName;
                this._port.BaudRate = comParam.BaudRate;

                this._port.Parity = comParam.Parity;
                this._port.DataBits = comParam.DataBits;
                this._port.StopBits = comParam.StopBits;

                this._port.RtsEnable = true;
                this._port.DtrEnable = true;

                this._port.ReceivedBytesThreshold = 10000;

                // open it
                this._port.Open();

                // create the thread for reading data from the port
                if (this._thrRead != null)
                {
                    this._thrRead.Abort();
                    this._thrRead.Join(500);
                    this._thrRead = null;
                }

                // load function of the thread for reading data from the port
                this._thrRead = new Thread(new ThreadStart(this.ReadData));
                this._thrRead.IsBackground = true;
                this._thrRead.Start();

                this.OnOpenPort?.Invoke(this, new ComParam());
                this.State = true;

            }
            catch 
            {
                              
                
            }
        }

        // Close COM port
        public void ClosePort()
        {
           

            try
            {
                this._port.DiscardInBuffer();

                this._port.DiscardOutBuffer();

                // close port
                this._port.Close();

                // destroy thread of reading
                if (this._thrRead != null)
                {
                    this._thrRead.Abort();
                    this._thrRead.Join(500);
                    this._thrRead = null;
                }

                // raise event
                // raise  event
                
                this.OnClosePort?.Invoke(this, new ComParam());
                this.State = false;
            }

            catch (System.Exception)
            {

            }

        }




        protected bool WriteData(byte[] bSend)
        {
            try
            {
                this._port.Write(bSend, 0, bSend.Length);

                // raise  event
                this.OnWriteByte?.Invoke(this, new ByteEventArgs(bSend.ToList()));

                return true;
            }
            catch  (Exception ex)
            {                
                return false;
            }

        }

        #region ReadFunction

        protected virtual void AddData(List<byte> data)
        { }

        // Read data (byte) from port (thread)
        private void ReadData()
        {
            byte[] bRead = new byte[_sizeRead] ;
            List<byte> lByte;


            // while port open
            while (true)
            {
                try
                {
                    // read data
                    var iReadByte = this._port.Read(bRead, 0, _sizeRead);

                    lByte = new List<byte>(iReadByte);
                    for (int i=0; i<iReadByte; i++)
                        lByte.Add(bRead[i]);


                    // if bytes was read
                    if (iReadByte > 0)
                    {
                        try
                        {
                            // raise  event
                            this.OnReadByte?.Invoke(this, new ByteEventArgs(lByte));

                            this.AddData(lByte);

                        }
                        catch (Exception ex)
                        {
                            //ClosePort();
                        }

                    }
                    else
                    {
                        this.ClosePort();
                    }

                   
                }

                catch 
                {
                   
                    this.ClosePort();
                   
                }
            }
        }


        #endregion
    }
}
