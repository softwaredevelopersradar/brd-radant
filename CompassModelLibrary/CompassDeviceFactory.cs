﻿

using CompassModelLibrary.Com;

namespace CompassModelLibrary
{
    using System;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Windows.Media;
    using ModelsTablesDBLib;
    using CompassModelLibrary.Devices;

    public class CompassDeviceFactory
    {

        private const string RRS1 = "RRS1";
        private const string RRS2 = "RRS2";
        private const string LPA = "LPA";
        private const string LPAU = "LPAU"; // управляемый

        public CompassDeviceFactory(LocalProperties properties)
        {
            this.localProperties = properties;
        }

        private LocalProperties localProperties { get; set; }


        private void LocalPropertiesOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(this.localProperties.ARD1)
                || e.PropertyName == nameof(this.localProperties.ARD2)
                || e.PropertyName == nameof(this.localProperties.ARD3))
            {

                //todo not sure about it.
            }
        }

        public struct SMeaning
        {
            public static string meaningSSTU = "SSTU";
            public static string meaningChangeRecord = "Change record";


            //public static void InitSMeaning()
            //{
            //    meaningAddRecord =;
            //    meaningChangeRecord ;
            //}
        }

        private string ChooseName(DeviceRSD deviceRSD)
        {
            switch (deviceRSD)
            {
                case DeviceRSD.SSTU:
                    return SMeaning.meaningSSTU;
                default:
                    return SMeaning.meaningChangeRecord;
            }
        }
        //MessageBox.Show(WpfApplication1.Properties.Resources.String1);
        public ObservableCollection<CompassModel> CreateDeviceObject()
        {
            var collection = new ObservableCollection<CompassModel>();
            if (this.localProperties.ARD1.State)
            {
                switch (localProperties.ARD1.Type)
                {
                    case TypeRSD.Radant:
                        collection.Add(new CompassModel(new ComBRD(localProperties.ARD1))
                        {
                            NameDevice = nameof(this.localProperties.ARD1),
                            Name = this.localProperties.ARD1.Device.ToString(),
                            Course = 0,
                            DColor = (Brush)new BrushConverter().ConvertFromString("#6b8e23"),
                            Correct = 0,
                            Tier = this.localProperties.ARD1.Level,
                        });

                        break;
                    case TypeRSD.RSD_20:
                        collection.Add(new CompassModel(new APUDevice(localProperties.ARD1))
                        {
                            NameDevice = nameof(this.localProperties.ARD1),
                            Name = this.localProperties.ARD1.Device.ToString(),
                            Course = 0,
                            DColor = (Brush)new BrushConverter().ConvertFromString("#6b8e23"),
                            Correct = 0,
                            Tier = this.localProperties.ARD1.Level,
                        });
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            if (this.localProperties.ARD2.State)
            {
                switch (localProperties.ARD2.Type)
                {
                    case TypeRSD.Radant:
                        collection.Add(new CompassModel(new ComBRD(localProperties.ARD2))
                        {
                            NameDevice = nameof(this.localProperties.ARD2),
                            Name = this.localProperties.ARD2.Device.ToString(),
                            Course = 0,
                            DColor = Brushes.OrangeRed,
                            Correct = 0,
                            Tier = this.localProperties.ARD2.Level,
                        });
                        break;
                    case TypeRSD.RSD_20:
                        collection.Add(new CompassModel(new APUDevice(localProperties.ARD2))
                        {
                            NameDevice = nameof(this.localProperties.ARD2),
                            Name = this.localProperties.ARD2.Device.ToString(),
                            Course = 0,
                            DColor = Brushes.OrangeRed,
                            Correct = 0,
                            Tier = this.localProperties.ARD2.Level,
                        });
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            if (this.localProperties.ARD3.State)
            {
                switch (localProperties.ARD3.Type)
                {
                    case TypeRSD.Radant:
                        collection.Add(new CompassModel(new ComBRD(localProperties.ARD3))
                        {
                            NameDevice = nameof(this.localProperties.ARD3),
                            Name = this.localProperties.ARD3.Device.ToString(),
                            Course = 0,
                            DColor = Brushes.DarkBlue,
                            Correct = 0,
                            Tier = this.localProperties.ARD3.Level,
                        });
                        break;
                    case TypeRSD.RSD_20:
                        collection.Add(new CompassModel(new APUDevice(localProperties.ARD3))
                        {
                            NameDevice = nameof(this.localProperties.ARD3),
                            Name = this.localProperties.ARD3.Device.ToString(),
                            Course = 0,
                            DColor = Brushes.DarkBlue,
                            Correct = 0,
                            Tier = this.localProperties.ARD3.Level,
                        });
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return collection;
        }

    }
}