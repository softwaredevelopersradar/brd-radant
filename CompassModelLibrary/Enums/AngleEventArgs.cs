﻿namespace CompassModelLibrary.Enums
{
    public class AngleEventArgs
    {

        public float _angle { get; set; }
        public float _elevation { get; set; }

        public AngleEventArgs(float Angle, float Elevation)
        {
            this._angle = Angle;
            this._elevation = Elevation;

        }
     
    }
}
