﻿namespace CompassModelLibrary.Enums
{
    using System;
    using System.Collections.Generic;

    public class ByteEventArgs : EventArgs
    {
        private List<byte> _Data { get; set; }

        public ByteEventArgs(List<byte> Data)
        {

            this._Data = Data;
        }
    }

}
