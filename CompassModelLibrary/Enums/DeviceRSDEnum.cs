﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompassModelLibrary.Enums
{
    public enum DeviceRSDEnum : byte
    {
        //[LocalizedDescription("RRS_Lincked", typeof(Resource))]
        RRS_Lincked = 0,
        RRS_PC = 1,
        SSTU = 2,
        LPA_5_10 = 3,
        LPA_5_7 = 4,
        LPA_5_9 = 5,
        LPA_10б = 6
    }
}
