﻿namespace CompassModelLibrary.Enums
{
    public enum DeviceName
    {
        RRS1,
        RRS2,
        LPA,
        LPAU
    }
}